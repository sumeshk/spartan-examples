NOTA BENE: TUTORIAL IS INCOMPLETE. RUNS BUT WITH ERRORS.

Sone content derived from the Swedish Univeristy of Agricultural Sciences

https://www.hadriengourle.com/tutorials/

Busco (Benchmark Universal Single Copy Orthologs) can be used to to find marker genes in a assembly. Marker genes are conserved 
across a range of species and finding intact conserved genes in the assembly would be a good indication of its quality.

The file `m_genitalium.fasta` is from the MEGAHIT job example. 

The file bacteria_odb9.tar.gz is available from:
http://busco.ezlab.org/v2/datasets/bacteria_odb9.tar.gz

Busco requires editing of a configuration file to operate. A suggested process is as follows:

mkdir Busco 

cd Busco 

module load foss/2019b ; module load busco/4.0.5-python-3.7.4 

cp  /usr/local/easybuild-2019/easybuild/software/mpi/gcc/8.3.0/openmpi/3.1.4/busco/4.0.5-python-3.7.4/config/config.ini my-busco.conf 

busco_configurator.py /usr/local/easybuild-2019/easybuild/software/mpi/gcc/8.3.0/openmpi/3.1.4/busco/4.0.5-python-3.7.4/config/config.ini my-busco.conf



