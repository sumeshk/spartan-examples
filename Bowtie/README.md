Derived from the Swedish Univeristy of Agricultural Sciences

https://www.hadriengourle.com/tutorials/

This file contains the sequence of the pO157 plasmid from the Sakai outbreak strain of E. coli O157.

Available from: curl -O -J -L https://osf.io/rnzbe/download

The example maps a prepared read set to a reference sequnce to the virulence plasmid to determine whether p0157 is present in the St Louis 
outbreak strain.




