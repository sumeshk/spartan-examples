# De-novo Genome Assembly with MEGAHIT

Derived from content from the Swedish University of Agricultural Sciences

https://www.hadriengourle.com/tutorials/

## Data collection

M. genitalium was sequenced using the MiSeq platform (2 * 150bp). The reads were deposited in the ENA Short Read Archive under the 
accession ERR486840

wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR486/ERR486840/ERR486840_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR486/ERR486840/ERR486840_2.fastq.gz



