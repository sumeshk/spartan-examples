
# Introduction

Guppy software supports MinIT and MinION instruments from Nanopore Technologies.

# Modules

You will need to load the following modules to run guppy/3.6.1

fosscuda/2019b
guppy/3.6.1

The module fosscuda/2019b will load: gcc/8.3.0  cuda/10.1.243  openmpi/3.1.4

Or, if you want to use even older versions from the 2015 to 2019 build system:

source /usr/local/module/spartan_old.sh
module av guppy

--------------------------------------------------------- /usr/local/easybuild/modules/all ----------------------------------------------------------
   Guppy/2.3.1-cpu    Guppy/2.3.1-rpm    Guppy/3.2.4 (D)    ont-guppy/3.1.5


# New Versions and Installation

The guppy licencese has changed since 2018. The license is between the user and ONT.

https://nanoporetech.com/sites/default/files/s3/terms/Nanopore-product-terms-and-conditions-nov2018-v2.pdf

As a result, we cannot perform a centralised installation and have it accessed through our modules system. It has to be installed in the 
user's home directory. We can assist in the installation. The following steps may suffice.

Firstly, register yourself as a guppy user with ONT and download the Linux64 tarball to your home directory.

Extract the tarball and edit and run our setup script (setup_guppy.sh) from your home directory when using it with the command:

source /home/$(whoami)/ont-guppy/setup_guppy.sh
 




