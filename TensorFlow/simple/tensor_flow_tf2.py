# Based on https://www.tensorflow.org/guide/using_gpu

import tensorflow as tf
tf.compat.v1.disable_eager_execution()

# Creates a graph -- force it to run on the GPU
with tf.device('/gpu:0'):
  a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3], name='a')
  b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2], name='b')
  c = tf.matmul(a, b)

# Creates a session with log_device_placement set to True.
config=tf.compat.v1.ConfigProto(log_device_placement=True)

with tf.compat.v1.Session(config=config) as sess:
  # Runs the op.
  print(sess.run(c))

