Derived from content from the University of Agricultural Sciences, Sweden

Scythe uses a Naive Bayesian approach to classify contaminant substrings in sequence reads. It considers quality information, which 
can make it robust in picking out 3'-end adapters, which often include poor quality bases.

The sample job submission script takes a selection of paired reads from enterohaemorrhagic E. coli (EHEC) of the serotype O157, a 
potentially fatal gastrointestinal pathogen. The sequenced bacterium was part of an outbreak investigation in the St. Louis area, 
USA in 2011.

The script has two components; first download the adapter. The run scythe on both of the paired read files.

