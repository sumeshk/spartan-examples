From: Lev Lafayette, Sequential and Parallel Programming with C and Fortran, VPAC, 2015. https://github.com/VPAC/seqpar

The most brief example derived from Erik Smistad is used here. It is a simple vector addition and the source code is available in the OpenCl 
sub-directory in the Resources directory. The example is a simple vector addition; two equal-sized lists of numbers (say, A and B) and added 
together to generate a list of, equal-sized, C. In serial code this can be achieved by a loop:

```
for(int i = 0; i < LIST_SIZE; i++) {
    C[i] = A[i] + B[i];
}
```

Whilst the loop itself is simple, there is an time dependency which cannot be resolved in a serial manner. In parallel the vector additions 
can occur simultaneously. In OpenCL a kernel is required that will run on the compute device. This is very much like the CUDA kernel, but is 
somewhat more complex in OpenCL. The following is just the kernel with a vector addition function.

```
__kernel void vector_add(__global const int *A, __global const int *B, __global int *C) {
    // Get the index of the current element to be processed
    int i = get_global_id(0);
    // Do the operation
    C[i] = A[i] + B[i];
}
```

The OpenCL API is defined in the `cl.h` header file. In the program system information is embodied and the devices to use in execution in the 
host program as follows:

```
// Get platform and device information
    cl_platform_id platform_id = NULL;
    cl_device_id device_id = NULL;   
    cl_uint ret_num_devices;
    cl_uint ret_num_platforms;
    cl_int ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);
    ret = clGetDeviceIDs( platform_id, CL_DEVICE_TYPE_DEFAULT, 1, 
            &device_id, &ret_num_devices);
```

One will also find the OpenCL context and a command queue.

```
// Create an OpenCL context
    cl_context context = clCreateContext( NULL, 1, &device_id, NULL, NULL, &ret);
// Create a command queue
    cl_command_queue command_queue = clCreateCommandQueue(context, device_id, 0, &ret);
```

Like in CUDA, memory needs to be transferred in a device. In OpenCL this is done by creating memory buffer objects.

```
// Create memory buffers on the device for each vector 
cl_mem a_mem_obj = clCreateBuffer(context, CL_MEM_READ_ONLY, 
	LIST_SIZE * sizeof(int), NULL, &ret);
cl_mem b_mem_obj = clCreateBuffer(context, CL_MEM_READ_ONLY,
	LIST_SIZE * sizeof(int), NULL, &ret);
cl_mem c_mem_obj = clCreateBuffer(context, CL_MEM_WRITE_ONLY, 
	LIST_SIZE * sizeof(int), NULL, &ret);
// Copy the lists A and B to their respective memory buffers
ret = clEnqueueWriteBuffer(command_queue, a_mem_obj, CL_TRUE, 0,
	LIST_SIZE * sizeof(int), A, 0, NULL, NULL);
ret = clEnqueueWriteBuffer(command_queue, b_mem_obj, CL_TRUE, 0, 
	LIST_SIZE * sizeof(int), B, 0, NULL, NULL);
```

OpenCL then needs a program object to be created and compiled.
 
```
// Create a program from the kernel source
cl_program program = clCreateProgramWithSource(context, 1, 
	(const char **)&source_str, (const size_t *)&source_size, &ret);
// Build the program
	ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);
``` 

Kernel arguments can then be created, set, and executed.

```
// Create the OpenCL kernel
cl_kernel kernel = clCreateKernel(program, "vector_add", &ret);
// Set the arguments of the kernel
ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&a_mem_obj);
ret = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&b_mem_obj);
ret = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&c_mem_obj);
// Execute the OpenCL kernel on the list
size_t global_item_size = LIST_SIZE; // Process the entire lists
size_t local_item_size = 64; // Divide work items into groups of 64
ret = clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL, 
	&global_item_size, &local_item_size, 0, NULL, NULL);
```

The memory buffer is read from the device to the local variable, and the results displayed.

```
// Read the memory buffer C on the device to the local variable C
int *C = (int*)malloc(sizeof(int)*LIST_SIZE);
ret = clEnqueueReadBuffer(command_queue, c_mem_obj, CL_TRUE, 0, 
	LIST_SIZE * sizeof(int), C, 0, NULL, NULL);
// Display the result to the screen
for(i = 0; i < LIST_SIZE; i++)
	printf("%d + %d = %d\n", A[i], B[i], C[i]);
```

Cleaning up is required in OpenCL

``` 
// Clean up
ret = clFlush(command_queue);
ret = clFinish(command_queue);
ret = clReleaseKernel(kernel);
ret = clReleaseProgram(program);
ret = clReleaseMemObject(a_mem_obj);
ret = clReleaseMemObject(b_mem_obj);
ret = clReleaseMemObject(c_mem_obj);
ret = clReleaseCommandQueue(command_queue);
ret = clReleaseContext(context);
free(A);
free(B);
free(C);
```

To make OpenCL run the kernel on the GPU the constant CL_DEVICE_TYPE_DEFAULT should be changed to CL_DEVICE_TYPE_GPU in line 43. To force it 
to run on CPU set it to CL_DEVICE_TYPE_CPU.

To compile the program use the following. An implementation of OpenCL will be required for the appropriate device. For example, NVIDIA, AMD 
have implementations of OpenCL for their GPUs.

`gcc -l OpenCL vector.c -o vector`

