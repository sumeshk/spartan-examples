#!/bin/bash

# To give your job a name, replace "MyJob" with an appropriate name
#SBATCH --job-name=Pilon-test.slurm

# Run on single CPU
#SBATCH --ntasks=1

# set your minimum acceptable walltime=days-hours:minutes:seconds
#SBATCH -t 0:15:00

# Specify your email address to be notified of progress.
# SBATCH --mail-user=youremailaddress@unimelb.edu.au
# SBATCH --mail-type=ALL

# Load the environment variables
module purge
module load foss/2019b
module load bowtie2/2.3.5.1
module load samtools/1.9
module load pilon/1.23-java-11.0.2


# The m_genitalium.fasta file is from final.contigs.fa generated in the MEGAHIT example.

# Before running Pilon itself, we have to align our reads against the assembly

bowtie2-build m_genitalium.fasta m_genitalium
bowtie2 -x m_genitalium -1 ERR486840_1.fastq.gz -2 ERR486840_2.fastq.gz | samtools view -bS -o m_genitalium.bam
samtools sort m_genitalium.bam -o m_genitalium.sorted.bam
samtools index m_genitalium.sorted.bam

# Then run Pilon to correct eventual mismatches in the assembly and write the new improved assembly to m_genitalium_improved.fasta

pilon --genome m_genitalium.fasta --frags m_genitalium.sorted.bam --output m_genitalium_improved


##DO NOT ADD/EDIT BEYOND THIS LINE##
##Job monitor command to list the resource usage
my-job-stats -a -n -s

