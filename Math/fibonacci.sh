#!/bin/bash
# Script for Fibonacci sequence
   
# Constants. First two numbers of the Fibonacci Series 
first=0 
second=1  

# User input for quantity of sequence
usage(){
echo "Enter an integer to witness the Fibonacci sequence!"
read input
if [ $((input)) != $input ]; then
    echo "Not a number! Exiting!"
    exit 0
fi
}

# Loop to display the series   
calculate() {
echo "The Fibonacci series is : "
for (( i=0; i<input; i++ )) 
do
    echo -n "$first "
    fn=$((first + second)) 
    first=$second 
    second=$fn 
done
echo; echo
}

main() {
        usage
        calculate
}

# Main function
main
exit

