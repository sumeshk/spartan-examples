Derived from content by the Swedish University of Agricultural Sciences

https://www.hadriengourle.com/tutorials/

Once the Prokka job has finished, examine each of its output files.

The GFF and GBK files contain all of the information about the features annotated (in different formats.)
The .txt file contains a summary of the number of features annotated.
The .faa file contains the protein sequences of the genes annotated.
The .ffn file contains the nucleotide sequences of the genes annotated.

The annotation can be visualised with Artemis (module act).
