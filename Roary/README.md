
This example partially from the Swedish University of Agricultural Science and the Genome annotation and Pangenome Analysis from the CBIB inSantiago, Chile. It illustrates how to determine a pan-genome from a collection of isolate genomes.

https://www.hadriengourle.com/tutorials/

It starts with a dataset of Escherichia coli from the article "Prediction of antibiotic resistance in Escherichia coli from large-scale 
pan-genome data", available at https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1006258

The CSV file has been obtained from https://doi.org/10.1371/journal.pcbi.1006258.s010

Random strains are selected from the file and read through enaGroupGet and fastq for quality and placed in their own directory (`reads`).

The strains are assembled with MEGAHIT and then annotated with Prokka.

All the .gff files are placed in the same folder and roary is run against them, getting the coding sequences, converting them into protein, 
creating pre-clusters, and checking for paralogs. Every isolate is taken and ordered by presence or absence of orthologs. The summary is in 
the file summary_statistics.txt along with a gene_presence_absence.csv includes the gene name and gene annotation, and whether a gene is 
present in a genome or not. Finally, a tree file is created.






