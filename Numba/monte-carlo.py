# Example derived from
# https://www.analyticsvidhya.com/blog/2021/04/numba-for-data-science-make-your-py-code-run-1000x-faster/

import random
from numba import jit,njit,vectorize

import time

def monte_carlo_pi(nsamples):
    acc = 0
    for i in range(nsamples):
        x = random.random()
        y = random.random()
        if (x ** 2 + y ** 2) < 1.0:
            acc += 1
    return 4.0 * acc / nsamples

# Standard Python

start = time.time()
monte_carlo_pi( 10000 )
end = time.time()
print(end - start)

# Numba JIT compilation, example actually slower!

start = time.time()
monte_carlo_pi_jit = jit() (monte_carlo_pi)
monte_carlo_pi_jit( 10000 )
end = time.time()
print(end - start)

# Object now compiled, now 1000x faster!

start = time.time()
monte_carlo_pi_jit( 10000 )
end = time.time()
print(end - start)



