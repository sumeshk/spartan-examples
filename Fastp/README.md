Derived from content by the Swedish University of Agricultural Sciences.

https://www.hadriengourle.com/tutorials/

This example investigates metagenomics data and retrieve draft genome from an assembled metagenome.

It uses a dataset published in 2017 in a study in dolphins, where fecal samples where prepared for viral metagenomics study. The 
dolphin had a self-limiting gastroenteritis of suspected viral origin.

Use FastQC to check the quality of the data, as well as fastp for trimming the bad quality part of the reads. 

Bowtie2 is used for removing the host sequences by mapping/aligning on the dolphin genome.


