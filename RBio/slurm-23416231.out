
R version 3.6.2 (2019-12-12) -- "Dark and Stormy Night"
Copyright (C) 2019 The R Foundation for Statistical Computing
Platform: x86_64-pc-linux-gnu (64-bit)

R is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under certain conditions.
Type 'license()' or 'licence()' for distribution details.

  Natural language support but running in an English locale

R is a collaborative project with many contributors.
Type 'contributors()' for more information and
'citation()' on how to cite R or R packages in publications.

Type 'demo()' for some demos, 'help()' for on-line help, or
'help.start()' for an HTML browser interface to help.
Type 'q()' to quit R.

> # Load packages
> library(dada2)
Loading required package: Rcpp
> library(ggplot2)
> library(phyloseq)
> library(phangorn)
Loading required package: ape
> library(DECIPHER)
Loading required package: Biostrings
Loading required package: BiocGenerics
Loading required package: parallel

Attaching package: ‘BiocGenerics’

The following objects are masked from ‘package:parallel’:

    clusterApply, clusterApplyLB, clusterCall, clusterEvalQ,
    clusterExport, clusterMap, parApply, parCapply, parLapply,
    parLapplyLB, parRapply, parSapply, parSapplyLB

The following objects are masked from ‘package:stats’:

    IQR, mad, sd, var, xtabs

The following objects are masked from ‘package:base’:

    anyDuplicated, append, as.data.frame, basename, cbind, colnames,
    dirname, do.call, duplicated, eval, evalq, Filter, Find, get, grep,
    grepl, intersect, is.unsorted, lapply, Map, mapply, match, mget,
    order, paste, pmax, pmax.int, pmin, pmin.int, Position, rank,
    rbind, Reduce, rownames, sapply, setdiff, sort, table, tapply,
    union, unique, unsplit, which, which.max, which.min

Loading required package: S4Vectors
Loading required package: stats4

Attaching package: ‘S4Vectors’

The following object is masked from ‘package:base’:

    expand.grid

Loading required package: IRanges

Attaching package: ‘IRanges’

The following object is masked from ‘package:phyloseq’:

    distance

Loading required package: XVector

Attaching package: ‘Biostrings’

The following object is masked from ‘package:ape’:

    complement

The following object is masked from ‘package:base’:

    strsplit

Loading required package: RSQLite
> packageVersion('dada2')
[1] ‘1.14.1’
> 
> # Check the dataset
> path <- 'MiSeq_SOP'
> list.files(path)
 [1] "F3D0_S188_L001_R1_001.fastq"         "F3D0_S188_L001_R2_001.fastq"        
 [3] "F3D1_S189_L001_R1_001.fastq"         "F3D1_S189_L001_R2_001.fastq"        
 [5] "F3D141_S207_L001_R1_001.fastq"       "F3D141_S207_L001_R2_001.fastq"      
 [7] "F3D142_S208_L001_R1_001.fastq"       "F3D142_S208_L001_R2_001.fastq"      
 [9] "F3D143_S209_L001_R1_001.fastq"       "F3D143_S209_L001_R2_001.fastq"      
[11] "F3D144_S210_L001_R1_001.fastq"       "F3D144_S210_L001_R2_001.fastq"      
[13] "F3D145_S211_L001_R1_001.fastq"       "F3D145_S211_L001_R2_001.fastq"      
[15] "F3D146_S212_L001_R1_001.fastq"       "F3D146_S212_L001_R2_001.fastq"      
[17] "F3D147_S213_L001_R1_001.fastq"       "F3D147_S213_L001_R2_001.fastq"      
[19] "F3D148_S214_L001_R1_001.fastq"       "F3D148_S214_L001_R2_001.fastq"      
[21] "F3D149_S215_L001_R1_001.fastq"       "F3D149_S215_L001_R2_001.fastq"      
[23] "F3D150_S216_L001_R1_001.fastq"       "F3D150_S216_L001_R2_001.fastq"      
[25] "F3D2_S190_L001_R1_001.fastq"         "F3D2_S190_L001_R2_001.fastq"        
[27] "F3D3_S191_L001_R1_001.fastq"         "F3D3_S191_L001_R2_001.fastq"        
[29] "F3D5_S193_L001_R1_001.fastq"         "F3D5_S193_L001_R2_001.fastq"        
[31] "F3D6_S194_L001_R1_001.fastq"         "F3D6_S194_L001_R2_001.fastq"        
[33] "F3D7_S195_L001_R1_001.fastq"         "F3D7_S195_L001_R2_001.fastq"        
[35] "F3D8_S196_L001_R1_001.fastq"         "F3D8_S196_L001_R2_001.fastq"        
[37] "F3D9_S197_L001_R1_001.fastq"         "F3D9_S197_L001_R2_001.fastq"        
[39] "filtered"                            "HMP_MOCK.v35.fasta"                 
[41] "Mock_S280_L001_R1_001.fastq"         "Mock_S280_L001_R2_001.fastq"        
[43] "mouse.dpw.metadata"                  "mouse.time.design"                  
[45] "silva_nr_v128_train_set.fa.gz"       "silva_species_assignment_v128.fa.gz"
[47] "stability.batch"                     "stability.files"                    
> 
> # Filter and Trim
> raw_forward <- sort(list.files(path, pattern="_R1_001.fastq",
+                                full.names=TRUE))
> 
> raw_reverse <- sort(list.files(path, pattern="_R2_001.fastq",
+                                full.names=TRUE))
> 
> # we also need the sample names
> sample_names <- sapply(strsplit(basename(raw_forward), "_"),
+                        `[`,  # extracts the first element of a subset
+                        1)
> 
> 
> # Initial plot read quality. Forward reads are good quality while the reverse are way worse.
> plotQualityProfile(raw_forward[1:2])
Scale for 'y' is already present. Adding another scale for 'y', which will
replace the existing scale.
> plotQualityProfile(raw_reverse[1:2])
Scale for 'y' is already present. Adding another scale for 'y', which will
replace the existing scale.
> 
> # Dada2 requires us to define the name of our output files
> 
> # place filtered files in filtered/ subdirectory
> filtered_path <- file.path(path, "filtered")
> 
> filtered_forward <- file.path(filtered_path,
+                               paste0(sample_names, "_R1_trimmed.fastq.gz"))
> 
> filtered_reverse <- file.path(filtered_path,
+                               paste0(sample_names, "_R2_trimmed.fastq.gz"))
> 
> # Use standard filtering parameters: maxN=0 (DADA22 requires no Ns), truncQ=2, rm.phix=TRUE and maxEE=2. The maxEE parameter sets the maximum 
> # number of “expected errors” allowed in a read, which according to the USEARCH authors is a better filter than simply averaging quality 
> # scores.
> 
> out <- filterAndTrim(raw_forward, filtered_forward, raw_reverse,
+                      filtered_reverse, truncLen=c(240,160), maxN=0,
+                      maxEE=c(2,2), truncQ=2, rm.phix=TRUE, compress=TRUE,
+                      multithread=TRUE)
> head(out)
                              reads.in reads.out
F3D0_S188_L001_R1_001.fastq       7793      7113
F3D1_S189_L001_R1_001.fastq       5869      5299
F3D141_S207_L001_R1_001.fastq     5958      5463
F3D142_S208_L001_R1_001.fastq     3183      2914
F3D143_S209_L001_R1_001.fastq     3178      2941
F3D144_S210_L001_R1_001.fastq     4827      4312
> 
> # Learn and plot the Error Rates
> 
> # The DADA2 algorithm depends on a parametric error model and every amplicon dataset has a slightly different error rate. The learnErrors of 
> # Dada2 learns the error model from the data and will help DADA2 to fits its method to your data.
> 
> errors_forward <- learnErrors(filtered_forward, multithread=TRUE)
33514080 total bases in 139642 reads from 20 samples will be used for learning the error rates.
> errors_reverse <- learnErrors(filtered_reverse, multithread=TRUE)
22342720 total bases in 139642 reads from 20 samples will be used for learning the error rates.
> 
> plotErrors(errors_forward, nominalQ=TRUE) +
+     theme_minimal()
Warning messages:
1: Transformation introduced infinite values in continuous y-axis 
2: Transformation introduced infinite values in continuous y-axis 
> 
> # Dereplication
> 
> # From the Dada2 documentation: Dereplication combines all identical sequencing reads into into “unique sequences” with a corresponding 
> # “abundance”: the number of reads with that unique sequence. Dereplication substantially reduces computation time by eliminating redundant 
> # comparisons.
> 
> derep_forward <- derepFastq(filtered_forward, verbose=TRUE)
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D0_R1_trimmed.fastq.gz
Encountered 1979 unique sequences from 7113 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D1_R1_trimmed.fastq.gz
Encountered 1639 unique sequences from 5299 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D141_R1_trimmed.fastq.gz
Encountered 1477 unique sequences from 5463 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D142_R1_trimmed.fastq.gz
Encountered 904 unique sequences from 2914 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D143_R1_trimmed.fastq.gz
Encountered 939 unique sequences from 2941 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D144_R1_trimmed.fastq.gz
Encountered 1267 unique sequences from 4312 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D145_R1_trimmed.fastq.gz
Encountered 1756 unique sequences from 6741 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D146_R1_trimmed.fastq.gz
Encountered 1438 unique sequences from 4560 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D147_R1_trimmed.fastq.gz
Encountered 3590 unique sequences from 15637 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D148_R1_trimmed.fastq.gz
Encountered 2762 unique sequences from 11413 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D149_R1_trimmed.fastq.gz
Encountered 3021 unique sequences from 12017 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D150_R1_trimmed.fastq.gz
Encountered 1566 unique sequences from 5032 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D2_R1_trimmed.fastq.gz
Encountered 3707 unique sequences from 18075 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D3_R1_trimmed.fastq.gz
Encountered 1479 unique sequences from 6250 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D5_R1_trimmed.fastq.gz
Encountered 1195 unique sequences from 4052 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D6_R1_trimmed.fastq.gz
Encountered 1832 unique sequences from 7369 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D7_R1_trimmed.fastq.gz
Encountered 1183 unique sequences from 4765 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D8_R1_trimmed.fastq.gz
Encountered 1382 unique sequences from 4871 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D9_R1_trimmed.fastq.gz
Encountered 1709 unique sequences from 6504 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/Mock_R1_trimmed.fastq.gz
Encountered 897 unique sequences from 4314 total sequences read.
> derep_reverse <- derepFastq(filtered_reverse, verbose=TRUE)
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D0_R2_trimmed.fastq.gz
Encountered 1660 unique sequences from 7113 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D1_R2_trimmed.fastq.gz
Encountered 1349 unique sequences from 5299 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D141_R2_trimmed.fastq.gz
Encountered 1335 unique sequences from 5463 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D142_R2_trimmed.fastq.gz
Encountered 853 unique sequences from 2914 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D143_R2_trimmed.fastq.gz
Encountered 880 unique sequences from 2941 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D144_R2_trimmed.fastq.gz
Encountered 1286 unique sequences from 4312 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D145_R2_trimmed.fastq.gz
Encountered 1803 unique sequences from 6741 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D146_R2_trimmed.fastq.gz
Encountered 1265 unique sequences from 4560 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D147_R2_trimmed.fastq.gz
Encountered 3414 unique sequences from 15637 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D148_R2_trimmed.fastq.gz
Encountered 2522 unique sequences from 11413 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D149_R2_trimmed.fastq.gz
Encountered 2771 unique sequences from 12017 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D150_R2_trimmed.fastq.gz
Encountered 1415 unique sequences from 5032 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D2_R2_trimmed.fastq.gz
Encountered 3290 unique sequences from 18075 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D3_R2_trimmed.fastq.gz
Encountered 1390 unique sequences from 6250 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D5_R2_trimmed.fastq.gz
Encountered 1134 unique sequences from 4052 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D6_R2_trimmed.fastq.gz
Encountered 1635 unique sequences from 7369 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D7_R2_trimmed.fastq.gz
Encountered 1084 unique sequences from 4765 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D8_R2_trimmed.fastq.gz
Encountered 1161 unique sequences from 4871 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/F3D9_R2_trimmed.fastq.gz
Encountered 1502 unique sequences from 6504 total sequences read.
Dereplicating sequence entries in Fastq file: MiSeq_SOP/filtered/Mock_R2_trimmed.fastq.gz
Encountered 732 unique sequences from 4314 total sequences read.
> # name the derep-class objects by the sample names
> names(derep_forward) <- sample_names
> names(derep_reverse) <- sample_names
> 
> # Sample inference
> # We are now ready to apply the core sequence-variant inference algorithm to the dereplicated data.
> 
> dada_forward <- dada(derep_forward, err=errors_forward, multithread=TRUE)
Sample 1 - 7113 reads in 1979 unique sequences.
Sample 2 - 5299 reads in 1639 unique sequences.
Sample 3 - 5463 reads in 1477 unique sequences.
Sample 4 - 2914 reads in 904 unique sequences.
Sample 5 - 2941 reads in 939 unique sequences.
Sample 6 - 4312 reads in 1267 unique sequences.
Sample 7 - 6741 reads in 1756 unique sequences.
Sample 8 - 4560 reads in 1438 unique sequences.
Sample 9 - 15637 reads in 3590 unique sequences.
Sample 10 - 11413 reads in 2762 unique sequences.
Sample 11 - 12017 reads in 3021 unique sequences.
Sample 12 - 5032 reads in 1566 unique sequences.
Sample 13 - 18075 reads in 3707 unique sequences.
Sample 14 - 6250 reads in 1479 unique sequences.
Sample 15 - 4052 reads in 1195 unique sequences.
Sample 16 - 7369 reads in 1832 unique sequences.
Sample 17 - 4765 reads in 1183 unique sequences.
Sample 18 - 4871 reads in 1382 unique sequences.
Sample 19 - 6504 reads in 1709 unique sequences.
Sample 20 - 4314 reads in 897 unique sequences.
> dada_reverse <- dada(derep_reverse, err=errors_reverse, multithread=TRUE)
Sample 1 - 7113 reads in 1660 unique sequences.
Sample 2 - 5299 reads in 1349 unique sequences.
Sample 3 - 5463 reads in 1335 unique sequences.
Sample 4 - 2914 reads in 853 unique sequences.
Sample 5 - 2941 reads in 880 unique sequences.
Sample 6 - 4312 reads in 1286 unique sequences.
Sample 7 - 6741 reads in 1803 unique sequences.
Sample 8 - 4560 reads in 1265 unique sequences.
Sample 9 - 15637 reads in 3414 unique sequences.
Sample 10 - 11413 reads in 2522 unique sequences.
Sample 11 - 12017 reads in 2771 unique sequences.
Sample 12 - 5032 reads in 1415 unique sequences.
Sample 13 - 18075 reads in 3290 unique sequences.
Sample 14 - 6250 reads in 1390 unique sequences.
Sample 15 - 4052 reads in 1134 unique sequences.
Sample 16 - 7369 reads in 1635 unique sequences.
Sample 17 - 4765 reads in 1084 unique sequences.
Sample 18 - 4871 reads in 1161 unique sequences.
Sample 19 - 6504 reads in 1502 unique sequences.
Sample 20 - 4314 reads in 732 unique sequences.
> 
> # inspect the dada-class object
> dada_forward[[1]]
dada-class: object describing DADA2 denoising results
128 sequence variants were inferred from 1979 input unique sequences.
Key parameters: OMEGA_A = 1e-40, OMEGA_C = 1e-40, BAND_SIZE = 16
> 
> # Merge Paired-end Reads
> # Now that the reads are trimmed, dereplicated and error-corrected we can merge them together
> 
> merged_reads <- mergePairs(dada_forward, derep_forward, dada_reverse,
+                            derep_reverse, verbose=TRUE)
6540 paired-reads (in 107 unique pairings) successfully merged out of 6891 (in 197 pairings) input.
5028 paired-reads (in 101 unique pairings) successfully merged out of 5190 (in 157 pairings) input.
4986 paired-reads (in 81 unique pairings) successfully merged out of 5267 (in 166 pairings) input.
2595 paired-reads (in 52 unique pairings) successfully merged out of 2754 (in 108 pairings) input.
2552 paired-reads (in 60 unique pairings) successfully merged out of 2784 (in 119 pairings) input.
3627 paired-reads (in 54 unique pairings) successfully merged out of 4109 (in 156 pairings) input.
6079 paired-reads (in 81 unique pairings) successfully merged out of 6514 (in 198 pairings) input.
3968 paired-reads (in 91 unique pairings) successfully merged out of 4388 (in 187 pairings) input.
14231 paired-reads (in 143 unique pairings) successfully merged out of 15355 (in 353 pairings) input.
10529 paired-reads (in 120 unique pairings) successfully merged out of 11165 (in 277 pairings) input.
11154 paired-reads (in 137 unique pairings) successfully merged out of 11797 (in 298 pairings) input.
4349 paired-reads (in 85 unique pairings) successfully merged out of 4802 (in 179 pairings) input.
17431 paired-reads (in 153 unique pairings) successfully merged out of 17812 (in 272 pairings) input.
5853 paired-reads (in 81 unique pairings) successfully merged out of 6099 (in 160 pairings) input.
3716 paired-reads (in 86 unique pairings) successfully merged out of 3894 (in 147 pairings) input.
6865 paired-reads (in 99 unique pairings) successfully merged out of 7191 (in 187 pairings) input.
4428 paired-reads (in 68 unique pairings) successfully merged out of 4605 (in 128 pairings) input.
4576 paired-reads (in 101 unique pairings) successfully merged out of 4739 (in 174 pairings) input.
6092 paired-reads (in 109 unique pairings) successfully merged out of 6315 (in 173 pairings) input.
4269 paired-reads (in 20 unique pairings) successfully merged out of 4281 (in 28 pairings) input.
> 
> # inspect the merger data.frame from the first sample
> head(merged_reads[[1]])
                                                                                                                                                                                                                                                      sequence
1 TACGGAGGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGCAGGCGGAAGATCAAGTCAGCGGTAAAATTGAGAGGCTCAACCTCTTCGAGCCGTTGAAACTGGTTTTCTTGAGTGAGCGAGAAGTATGCGGAATGCGTGGTGTAGCGGTGAAATGCATAGATATCACGCAGAACTCCGATTGCGAAGGCAGCATACCGGCGCTCAACTGACGCTCATGCACGAAAGTGTGGGTATCGAACAGG
2 TACGGAGGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGTAGGCGGCCTGCCAAGTCAGCGGTAAAATTGCGGGGCTCAACCCCGTACAGCCGTTGAAACTGCCGGGCTCGAGTGGGCGAGAAGTATGCGGAATGCGTGGTGTAGCGGTGAAATGCATAGATATCACGCAGAACCCCGATTGCGAAGGCAGCATACCGGCGCCCTACTGACGCTGAGGCACGAAAGTGCGGGGATCAAACAGG
3 TACGGAGGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGTAGGCGGGCTGTTAAGTCAGCGGTCAAATGTCGGGGCTCAACCCCGGCCTGCCGTTGAAACTGGCGGCCTCGAGTGGGCGAGAAGTATGCGGAATGCGTGGTGTAGCGGTGAAATGCATAGATATCACGCAGAACTCCGATTGCGAAGGCAGCATACCGGCGCCCGACTGACGCTGAGGCACGAAAGCGTGGGTATCGAACAGG
4 TACGGAGGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGTAGGCGGGCTTTTAAGTCAGCGGTAAAAATTCGGGGCTCAACCCCGTCCGGCCGTTGAAACTGGGGGCCTTGAGTGGGCGAGAAGAAGGCGGAATGCGTGGTGTAGCGGTGAAATGCATAGATATCACGCAGAACCCCGATTGCGAAGGCAGCCTTCCGGCGCCCTACTGACGCTGAGGCACGAAAGTGCGGGGATCGAACAGG
5 TACGGAGGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGCAGGCGGACTCTCAAGTCAGCGGTCAAATCGCGGGGCTCAACCCCGTTCCGCCGTTGAAACTGGGAGCCTTGAGTGCGCGAGAAGTAGGCGGAATGCGTGGTGTAGCGGTGAAATGCATAGATATCACGCAGAACTCCGATTGCGAAGGCAGCCTACCGGCGCGCAACTGACGCTCATGCACGAAAGCGTGGGTATCGAACAGG
6 TACGGAGGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGTAGGCGGGATGCCAAGTCAGCGGTAAAAAAGCGGTGCTCAACGCCGTCGAGCCGTTGAAACTGGCGTTCTTGAGTGGGCGAGAAGTATGCGGAATGCGTGGTGTAGCGGTGAAATGCATAGATATCACGCAGAACTCCGATTGCGAAGGCAGCATACCGGCGCCCTACTGACGCTGAGGCACGAAAGCGTGGGTATCGAACAGG
  abundance forward reverse nmatch nmismatch nindel prefer accept
1       579       1       1    148         0      0      1   TRUE
2       470       2       2    148         0      0      2   TRUE
3       449       3       4    148         0      0      1   TRUE
4       430       4       3    148         0      0      2   TRUE
5       345       5       6    148         0      0      1   TRUE
6       282       6       5    148         0      0      2   TRUE
> 
> # Construct Sequence Table
> # Construct a sequence table of our mouse samples, a higher-resolution version of the OTU table produced by traditional methods.
> 
> seq_table <- makeSequenceTable(merged_reads)
> dim(seq_table)
[1]  20 293
> 
> # inspect distribution of sequence lengths
> table(nchar(getSequences(seq_table)))

251 252 253 254 255 
  1  88 196   6   2 
> 
> # Remove Chimeras
> # The dada method used earlier removes substitutions and indel errors but chimeras remain.
> 
> seq_table_nochim <- removeBimeraDenovo(seq_table, method='consensus',
+                                        multithread=TRUE, verbose=TRUE)
Identified 61 bimeras out of 293 input sequences.
> dim(seq_table_nochim)
[1]  20 232
> 
> # which percentage of our reads did we keep?
> sum(seq_table_nochim) / sum(seq_table)
[1] 0.964064
> 
> # Check the number of reads that made it through each step in the pipeline
> 
> get_n <- function(x) sum(getUniques(x))
> 
> track <- cbind(out, sapply(dada_forward, get_n), sapply(merged_reads, get_n),
+                rowSums(seq_table), rowSums(seq_table_nochim))
> 
> colnames(track) <- c('input', 'filtered', 'denoised', 'merged', 'tabled',
+                      'nonchim')
> rownames(track) <- sample_names
> head(track)
       input filtered denoised merged tabled nonchim
F3D0    7793     7113     6976   6540   6540    6528
F3D1    5869     5299     5227   5028   5028    5017
F3D141  5958     5463     5331   4986   4986    4863
F3D142  3183     2914     2799   2595   2595    2521
F3D143  3178     2941     2822   2552   2552    2518
F3D144  4827     4312     4151   3627   3627    3488
> 
> # Assign Taxonomy
> # Aassign taxonomy to the sequences using the SILVA database
> 
> taxa <- assignTaxonomy(seq_table_nochim,
+                        'MiSeq_SOP/silva_nr_v128_train_set.fa.gz',
+                        multithread=TRUE)
> taxa <- addSpecies(taxa, 'MiSeq_SOP/silva_species_assignment_v128.fa.gz')
> 
> # for inspecting the classification
> 
> taxa_print <- taxa  # removing sequence rownames for display only
> rownames(taxa_print) <- NULL
> head(taxa_print)
     Kingdom    Phylum          Class         Order          
[1,] "Bacteria" "Bacteroidetes" "Bacteroidia" "Bacteroidales"
[2,] "Bacteria" "Bacteroidetes" "Bacteroidia" "Bacteroidales"
[3,] "Bacteria" "Bacteroidetes" "Bacteroidia" "Bacteroidales"
[4,] "Bacteria" "Bacteroidetes" "Bacteroidia" "Bacteroidales"
[5,] "Bacteria" "Bacteroidetes" "Bacteroidia" "Bacteroidales"
[6,] "Bacteria" "Bacteroidetes" "Bacteroidia" "Bacteroidales"
     Family                      Genus         Species       
[1,] "Bacteroidales_S24-7_group" NA            NA            
[2,] "Bacteroidales_S24-7_group" NA            NA            
[3,] "Bacteroidales_S24-7_group" NA            NA            
[4,] "Bacteroidales_S24-7_group" NA            NA            
[5,] "Bacteroidaceae"            "Bacteroides" "acidifaciens"
[6,] "Bacteroidales_S24-7_group" NA            NA            
> 
> # Phylogenetic Tree
> # DADA2 is reference-free so we have to build the tree ourselves
> 
> # Align our sequences
> 
> sequences <- getSequences(seq_table)
> names(sequences) <- sequences  # this propagates to the tip labels of the tree
> alignment <- AlignSeqs(DNAStringSet(sequences), anchor=NA)
Determining distance matrix based on shared 8-mers:
================================================================================

Time difference of 0.45 secs

Clustering into groups by similarity:
================================================================================

Time difference of 0.07 secs

Aligning Sequences:
================================================================================

Time difference of 2.52 secs

Iteration 1 of 2:

Determining distance matrix based on alignment:
================================================================================

Time difference of 0.03 secs

Reclustering into groups by similarity:
================================================================================

Time difference of 0.06 secs

Realigning Sequences:
================================================================================

Time difference of 1.08 secs

Iteration 2 of 2:

Determining distance matrix based on alignment:
================================================================================

Time difference of 0.03 secs

Reclustering into groups by similarity:
================================================================================

Time difference of 0.07 secs

Realigning Sequences:
================================================================================

Time difference of 0.14 secs

> 
> # Build a neighbour-joining tree then fit a maximum likelihood tree using the neighbour-joining tree as a starting point.
> 
> phang_align <- phyDat(as(alignment, 'matrix'), type='DNA')
> dm <- dist.ml(phang_align)
> treeNJ <- NJ(dm)  # note, tip order != sequence order
> fit = pml(treeNJ, data=phang_align)
negative edges length changed to 0!
> 
> ## negative edges length changed to 0!
> 
> fitGTR <- update(fit, k=4, inv=0.2)
> fitGTR <- optim.pml(fitGTR, model='GTR', optInv=TRUE, optGamma=TRUE,
+                     rearrangement = 'stochastic',
+                     control = pml.control(trace = 0))
> detach('package:phangorn', unload=TRUE)
> 
> # Phyloseq. First load the metadata
> 
> sample_data <- read.table(
+     'https://hadrieng.github.io/tutorials/data/16S_metadata.txt',
+     header=TRUE, row.names="sample_name")
> 
> # Construct a phyloseq object from our output and newly created metadata
> 
> physeq <- phyloseq(otu_table(seq_table_nochim, taxa_are_rows=FALSE),
+                    sample_data(sample_data),
+                    tax_table(taxa),
+                    phy_tree(fitGTR$tree))
> # remove mock sample
> 
> physeq <- prune_samples(sample_names(physeq) != 'Mock', physeq)
> physeq
phyloseq-class experiment-level object
otu_table()   OTU Table:         [ 232 taxa and 19 samples ]
sample_data() Sample Data:       [ 19 samples by 4 sample variables ]
tax_table()   Taxonomy Table:    [ 232 taxa by 7 taxonomic ranks ]
phy_tree()    Phylogenetic Tree: [ 232 tips and 230 internal nodes ]
> 
> # Look at the alpha diversity of our samples
> 
> plot_richness(physeq, x='day', measures=c('Shannon', 'Fisher'), color='when') +
+     theme_minimal()
Warning message:
In estimate_richness(physeq, split = TRUE, measures = measures) :
  The data you have provided does not have
any singletons. This is highly suspicious. Results of richness
estimates (for example) are probably unreliable, or wrong, if you have already
trimmed low-abundance taxa from the data.

We recommended that you find the un-trimmed data and retry.
> 
> # Ordination methods (beta diversity)
> # Perform an MDS with euclidean distance (mathematically equivalent to a PCA)
> 
> ord <- ordinate(physeq, 'MDS', 'euclidean')
> plot_ordination(physeq, ord, type='samples', color='when',
+                 title='PCA of the samples from the MiSeq SOP') +
+     theme_minimal()
> 
> # With the Bray-Curtis distance
> 
> ord <- ordinate(physeq, 'NMDS', 'bray')
Square root transformation
Wisconsin double standardization
Run 0 stress 0.08253503 
Run 1 stress 0.08346242 
Run 2 stress 0.239705 
Run 3 stress 0.08253505 
... Procrustes: rmse 1.409104e-05  max resid 2.82232e-05 
... Similar to previous best
Run 4 stress 0.08346242 
Run 5 stress 0.08346242 
Run 6 stress 0.08346242 
Run 7 stress 0.08346242 
Run 8 stress 0.1490855 
Run 9 stress 0.08346242 
Run 10 stress 0.08253505 
... Procrustes: rmse 4.504665e-05  max resid 0.0001497247 
... Similar to previous best
Run 11 stress 0.08253503 
... New best solution
... Procrustes: rmse 8.803591e-06  max resid 2.667612e-05 
... Similar to previous best
Run 12 stress 0.1388554 
Run 13 stress 0.08253504 
... Procrustes: rmse 1.734312e-05  max resid 5.78918e-05 
... Similar to previous best
Run 14 stress 0.1551181 
Run 15 stress 0.150697 
Run 16 stress 0.08253503 
... Procrustes: rmse 8.796457e-06  max resid 2.798993e-05 
... Similar to previous best
Run 17 stress 0.08346242 
Run 18 stress 0.08253503 
... New best solution
... Procrustes: rmse 4.356703e-06  max resid 1.231083e-05 
... Similar to previous best
Run 19 stress 0.08346242 
Run 20 stress 0.154452 
*** Solution reached
> plot_ordination(physeq, ord, type='samples', color='when',
+                 title='PCA of the samples from the MiSeq SOP') +
+     theme_minimal()
> 
> # Distribution of the most abundant families
> 
> top20 <- names(sort(taxa_sums(physeq), decreasing=TRUE))[1:20]
> physeq_top20 <- transform_sample_counts(physeq, function(OTU) OTU/sum(OTU))
> physeq_top20 <- prune_taxa(top20, physeq_top20)
> plot_bar(physeq_top20, x='day', fill='Family') +
+     facet_wrap(~when, scales='free_x') +
+     theme_minimal()
> 
> # Place them in a tree
> 
> bacteroidetes <- subset_taxa(physeq, Phylum %in% c('Bacteroidetes'))
> plot_tree(bacteroidetes, ladderize='left', size='abundance',
+           color='when', label.tips='Family')
> 
