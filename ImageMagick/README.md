This is an example of using ImageMagick on the cluster. The job submission script launched the script watermark.sh.

The file, watermark.sh, contains two functions. The first separates original jpg files in portrait and landscape directories. The second 
applies a different watermark to each directory (in this example they are the same, but you get the idea)...

Sample image files are from my own collection. The watermark is not.

LL202010105
